const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const port = process.env.PORT || 3000;

const sysInfo = {
  startTime: new Date(),
  numConnections: 0,
  maxNumConnections: 0
};

//11-15, 21-25, 31-35, 41-46
let tables = [...Array(20).keys()].map(i => {
  return { number: (Math.floor(i / 5) + 1) * 10 + 1 + (i % 5), ready: false, start: null };
});
tables.push({ number: 46, ready: false, start: null });

app.use(express.static('public'));
app.get('/moment.min.js', (req, res) => res.sendFile(`${__dirname}/node_modules/moment/min/moment.min.js`));

io.on('connection', (socket) => {
  console.log(`User connected.  ${++sysInfo.numConnections} connections.`);
  sysInfo.maxNumConnections = Math.max(sysInfo.maxNumConnections, sysInfo.numConnections);

  io.emit('sysInfo', sysInfo);
  io.emit('tables', tables);

  socket.on('disconnect', () => {
    console.log(`User disconnected.  ${--sysInfo.numConnections} connections.`);
    sysInfo.maxNumConnections = Math.max(sysInfo.maxNumConnections, sysInfo.numConnections);
    io.emit('sysInfo', sysInfo);
  });

  socket.on('toggle', num => {
    let table = tables.find(t => t.number === +num);
    table.ready = !table.ready;
    table.start = table.ready ? Date.now() : null;
    console.log(`Toggling table #${table.number} to ready: ${table.ready}.`);
    io.emit('table', table);
  });
});

http.listen(port, () => console.log(`Listening on port: ${port}.`));

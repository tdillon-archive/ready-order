#!/bin/bash
echo '[INFO] set -e'
set -e

echo '[INFO] pkill node, only if node process is running'
if pgrep node; then pkill node; fi

echo '[INFO] ls -la ~'
ls -la ~

echo '[INFO] rm -rf ready-order/*'
rm -rf ready-order/*

echo '[INFO] ls -la ready-order'
ls -la ready-order

echo '[INFO] tar -xzf ready-order.tar.gz -C ready-order;'
tar -xzf ready-order.tar.gz -C ready-order;

echo '[INFO] ls -la ready-order'
ls -la ready-order

echo '[INFO] cd ready-order'
cd ready-order

echo '[INFO] npm i'
npm i

echo '[INFO] nohup node index.js &'
nohup node index.js &

echo '[INFO] cd ..'
cd ..

echo '[INFO] rm *.tar.gz install.sh'
rm *.tar.gz install.sh

echo '[INFO] Finished deployment.'
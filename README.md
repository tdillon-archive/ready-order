# Ready Order

```shell
git clone https://gitlab.com/tdillon/ready-order.git
cd ready-order
npm i
npm start
```

- Open browser to `localhost:3000`
- Open browser to `localhost:3000/hallway`

## Deployment

- Find the Node process and kill it.

  ```shell
  top
  # L -> node -> q
  kill ####
  ```

- Update application

  ```shell
  cd ready-order
  git pull
  npm i
  ```

- Start temporarily

  ```shell
  npm start

  # or

  node index.js
  ```

- Start permatently

  ```shell
  # TODO
  ```
